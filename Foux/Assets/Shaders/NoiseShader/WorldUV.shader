﻿Shader "Faux/WorldUV" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_CutTex ("CutTexture", 2D) = "white" {}
		_Cutoff("Alpha cutoff", Range(0,2)) = 1
		_Res("Samples", Range(0.5,10)) = 0.5
	}

	SubShader {
		Tags{ "Queue" = "Overlay" "RenderType" = "TransparentCutout" "IgnoreProjector" = "True" }

		LOD 200
		Cull Off
		CGPROGRAM

		#pragma surface surf Lambert addshadow
		#include "noiseSimplex.cginc"

		sampler2D _MainTex;
		sampler2D _CutTex;

		struct Input {
			float2 uv_MainTex;
			float _Cutoff;
			float3 worldPos;
			float3 screenPos;
		};

		fixed4 _Color;
		float _Cutoff;
		float _Res;
		float _Radius;

		void surf (Input IN, inout SurfaceOutput o) {

			clip((tex2D (_CutTex, IN.worldPos)) * snoise(IN.worldPos * _Res) - (_Cutoff - IN.screenPos / 2) );
			o.Albedo = tex2D (_MainTex,IN.uv_MainTex) * _Color;

			o.Alpha = _Cutoff;
		}
	
		ENDCG
	}
	FallBack "Diffuse"
}