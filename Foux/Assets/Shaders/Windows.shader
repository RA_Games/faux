﻿Shader "Faux/Facades" {
	Properties {
		_Color ("Main Color", Color) = (1, 1, 1, 1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Cutoff("Amount", Range(0,2)) = 1
	}

	SubShader {
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" "IgnoreProjector" = "True" }

		LOD 200
		Cull Off
		CGPROGRAM

		#pragma surface surf Lambert alpha

		sampler2D _MainTex;

		struct Input {
			float2 uv_MainTex;
			half4 screenPos;
		};

		float _Cutoff;
		float4 _Color;

		void surf (Input IN, inout SurfaceOutput o) {

			half4 c = tex2D (_MainTex, IN.uv_MainTex);

			o.Albedo = c.rgb * _Color;

			_Cutoff -= IN.screenPos.z / 2;

			o.Alpha = c - _Cutoff;



		}
	
		ENDCG
	}
	FallBack "Diffuse"
}