﻿using UnityEngine;
using System.Collections;

public class Traffic_Light : MonoBehaviour {

	public Texture lightStatus_RED, lightStatus_YELLOW, lightStatus_GREEN;

	Material current_mat;

	public enum type
	{
		RED,
		YELLOW,
		GREEN
	}

	public type status;

	void OnEnable()
	{
		current_mat = GetComponent<Renderer> ().material;
	}

	IEnumerator Start ()
	{
		bool isGreen = false;

		switch (status) 
		{
		case type.RED:
			yield return new WaitForSeconds (10);
			current_mat.SetTexture ("_EmissionMap", lightStatus_RED);
			isGreen = false;
			status = type.YELLOW;
			break;

		case type.YELLOW:
			yield return new WaitForSeconds (7);
			current_mat.SetTexture ("_EmissionMap", lightStatus_YELLOW);

			if (isGreen)
				status = type.RED;
			else
				status = type.GREEN;

			break;

		case type.GREEN:
			yield return new WaitForSeconds (10);
			current_mat.SetTexture ("_EmissionMap", lightStatus_GREEN);
			isGreen = true;
			status = type.YELLOW;
			break;
		}

		StartCoroutine (Start());
	}
}
