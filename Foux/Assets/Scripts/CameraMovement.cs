﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class CameraMovement : MonoBehaviour {

	static CameraMovement instance;

	public float cameraSpeed = 2.5f;

	public static Transform player;
	static Vector3 target;
	static Transform cam;
	 
	enum type
	{
		Player,
		Interest,
		Zoom
	}

	public static bool isAffected;

	static type focus;
	static float zoom;

	static Vector3 zoomFocus;

	void Start()
	{
		player = GameObject.FindGameObjectWithTag ("Player").transform;
		cam = transform;
		instance = this;

		ResetTarget (0);
	}

	void FixedUpdate () 
	{
		switch(focus)
		{
		case type.Player:
			transform.position = Vector3.Lerp (transform.position, new Vector3(player.position.x, player.position.y + 1, -8), cameraSpeed);
			break;
		case type.Interest:
			transform.position = Vector3.Lerp (transform.position, new Vector3(target.x, target.y, -8), cameraSpeed * Time.deltaTime);
			break;
		case type.Zoom:
			transform.position = Vector3.Lerp (transform.position, zoomFocus, cameraSpeed * Time.deltaTime);
			break;
		}
	}

	public static void SetTarget(Vector2 value)
	{		
		focus = type.Interest;
		isAffected = true;
		target = new Vector3(value.x,value.y, -8);
	}

	public static void SetZoom(Vector3 zoom)
	{
		focus = type.Zoom;
		isAffected = true;
		zoomFocus = player.position + new Vector3 (zoom.x, cam.transform.position.y + zoom.y, cam.transform.position.z + zoom.z);
	}

	public static IEnumerator ResetTarget(float delay)
	{
		yield return new WaitForSeconds (delay);
		while(cam.transform.position.z < -8)
		{
			cam.transform.position = new Vector3 (cam.transform.position.x, cam.transform.position.y, cam.transform.position.z + (Time.deltaTime * 10));
			yield return new WaitForSeconds (Time.deltaTime);
		}

		focus = type.Player;
		isAffected = false;
	}

	public static IEnumerator ChangueSpeed(float newSpeed)
	{
		if(instance.cameraSpeed < newSpeed)
		{
			while(instance.cameraSpeed < newSpeed)
			{
				instance.cameraSpeed += Time.deltaTime;
				yield return new WaitForSeconds (Time.deltaTime);
			}
		}else
		{
			while(instance.cameraSpeed > newSpeed)
			{
				instance.cameraSpeed -= Time.deltaTime;
				yield return new WaitForSeconds (Time.deltaTime);
			}
		}
	}
}
