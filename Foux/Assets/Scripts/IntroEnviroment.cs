﻿using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]
public class IntroEnviroment : MonoBehaviour {

	public Color fog_color;

	[Range(0,0.05f)]
	public float fog_density;

	Material skybox;
	[Range(0,5)]
	public float atmosph;
	public Color sky_tint;

	public Transform sun;
	public Quaternion rotation;

	void OnEnable()
	{
		skybox = RenderSettings.skybox;
		sky_tint = skybox.GetColor ("_SkyTint");
		atmosph = skybox.GetFloat ("_AtmosphereThickness");
		rotation = sun.rotation;
		fog_color = RenderSettings.fogColor;
		fog_density = RenderSettings.fogDensity;
	}

	void Update () 
	{
		RenderSettings.fogColor = fog_color;
		RenderSettings.fogDensity = fog_density;

		skybox.SetFloat ("_AtmosphereThickness", atmosph);
		skybox.SetColor ("_SkyTint", sky_tint);

		sun.rotation = rotation;
	}
}
