﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class QuestType {
	
	public enum type
	{
		Dialogue,
		Point,
		Event,
		Area
	}

	public type questType;
		
	public QuestDialogue qDialogue;

	public QuestPoint qPoint;

	public QuestEvent qEvent;

	public QuestArea qArea;
}
