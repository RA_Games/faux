﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class E_FadeCamera : MonoBehaviour, IEvent {

	Image source;

	public bool isVisible;

	void Start () 
	{
		source = GetComponent<Image> ();
	}

	public IEnumerator RunEvent () 
	{
		if (isVisible) 
		{
			source.color = new Color (0,0,0,0);

			StartCoroutine (FadeIn());
		} else 
		{
			source.color = new Color (0,0,0,1);

			StartCoroutine (FadeOut());
		}

		yield return null;
	}

	IEnumerator FadeOut () 
	{
		while(source.color.a > 0)
		{
			source.color = new Color (0,0,0,source.color.a - Time.deltaTime);
			yield return new WaitForSeconds (Time.deltaTime * 4);
		}

		isVisible = true;

		GetComponent<QuestEvent> ().quest.Complete ();
	}

	IEnumerator FadeIn () 
	{
		while(source.color.a < 1)
		{
			source.color = new Color (0,0,0,source.color.a + Time.deltaTime);
			yield return new WaitForSeconds (Time.deltaTime * 4);
		}

		isVisible = false;

		GetComponent<QuestEvent> ().Complete ();
	}


}
