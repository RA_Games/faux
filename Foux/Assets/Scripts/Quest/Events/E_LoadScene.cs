﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

[RequireComponent(typeof(QuestEvent))]
public class E_LoadScene : MonoBehaviour, IEvent {

	public Material skybox;

	public enum names
	{
		Scene_1,
		Scene_2
	}

	public names scene;

	public IEnumerator RunEvent()
	{
		AsyncOperation asyn = SceneManager.LoadSceneAsync (scene.ToString(),LoadSceneMode.Additive);

		yield return asyn;

		DontDestroyOnLoad (Camera.main);
		DontDestroyOnLoad (CameraMovement.player);

		RenderSettings.skybox = skybox;

		DynamicGI.UpdateEnvironment ();
	}

}
