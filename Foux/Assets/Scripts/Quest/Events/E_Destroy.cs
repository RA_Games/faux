﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(QuestEvent))]
public class E_Destroy : MonoBehaviour, IEvent {

	public float delay;

	public List<GameObject> list = new List<GameObject> ();

	public IEnumerator RunEvent()
	{	
		yield return new WaitForSeconds (delay);

		foreach(GameObject go in list)
		{
			Destroy (go);
		}

		GetComponent<QuestEvent> ().quest.Complete ();

		Destroy (gameObject);

		yield return null;
	}
}
