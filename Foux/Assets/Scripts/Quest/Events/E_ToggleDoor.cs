﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(QuestEvent))]
public class E_ToggleDoor : MonoBehaviour, IEvent{
	
	public IEnumerator RunEvent()
	{	
		GetComponent<IDoor> ().OpenDoor ();
		GetComponent<QuestEvent> ().Complete ();

		GetComponent<QuestEvent> ().Complete();

		yield return null;
	}
}
