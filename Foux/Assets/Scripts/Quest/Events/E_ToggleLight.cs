﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(QuestEvent))]
public class E_ToggleLight : MonoBehaviour, IEvent {

	public bool isOn;
	#pragma warning disable 

	[SerializeField] Material light_ON;
	[SerializeField] Material light_OFF;

	#pragma warning restore 

	private Renderer bulb;
	private Light light_component;

	void Start()
	{
		light_component = GetComponentInChildren<Light> ();
		bulb = transform.FindChild ("Bulb").GetComponent<Renderer> ();

		if(isOn)
		{
			light_component.enabled = true;
			bulb.material = light_ON;
		}
	}

	public IEnumerator RunEvent()
	{	
		int iterations = Random.Range (3, 6);
		
		if(isOn)
		{
			while(iterations > 0)
			{
				light_component.enabled = false;
				bulb.material = light_OFF;

				yield return new WaitForSeconds (Time.deltaTime / 2);

				light_component.enabled = true;
				bulb.material = light_ON;

				yield return new WaitForSeconds (Time.deltaTime / 2);

				light_component.enabled = false;
				bulb.material = light_OFF;

				iterations--;

				yield return null;
			}
		}else
		{
			while(iterations > 0)
			{
				light_component.enabled = true;
				bulb.material = light_ON;

				yield return new WaitForSeconds (Time.deltaTime / 2);

				light_component.enabled = false;
				bulb.material = light_OFF;

				yield return new WaitForSeconds (Time.deltaTime / 2);

				light_component.enabled = true;
				bulb.material = light_ON;

				iterations--;

				yield return null;
			}
		}

		GetComponent<QuestEvent> ().quest.Complete ();
	}
}
