﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(QuestEvent))]
public class E_Teleport : MonoBehaviour, IEvent {

	private Transform player;
	private Transform cam;

	void Start()
	{
		player = GameObject.Find ("Player").transform;

		cam = Camera.main.transform;
	}

	public IEnumerator RunEvent()
	{	
		player.position = transform.position;

		cam.position = new Vector2 (transform.position.x,0);

		yield return new WaitForSeconds (0.3f);

		GetComponent<QuestEvent> ().Complete ();


	}

}
