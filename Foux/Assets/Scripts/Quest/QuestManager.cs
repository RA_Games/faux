﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class QuestManager : MonoBehaviour {

	//public List<Quest> quests = new List<Quest> ();
	public List<Quest> quests = new List<Quest> ();

	public static Quest current;

	void Awake()
	{
		foreach(Quest q in quests)
		{
			q.text = Translate.GetTranslation () [q.id];
		}
	}

	IEnumerator Start()
	{		
		foreach(Quest q in quests)
		{
			current = q;
			
			switch(q.questType.questType)
			{
			case QuestType.type.Dialogue:
				q.questType.qDialogue.quest = q;
				StartCoroutine(q.questType.qDialogue.StartDialogue(10));
				break;

			case QuestType.type.Point:
				q.questType.qPoint.quest = q;
				break;

			case QuestType.type.Area:
				q.questType.qArea.quest = q;
				break;

			case QuestType.type.Event:
				q.questType.qEvent.quest = q;
				q.questType.qEvent.StartEvent ();
				break;
			}

			while(!q.isComplete)
			{
				yield return null;
			}
		}
	}
}
