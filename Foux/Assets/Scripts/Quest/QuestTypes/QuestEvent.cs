﻿using UnityEngine;
using System.Collections;

public class QuestEvent : MonoBehaviour {

	[System.NonSerialized] public Quest quest;

	public IEvent e;

	public void StartEvent()
	{
		e = gameObject.GetComponent<IEvent> ();

		StartCoroutine (e.RunEvent());
	}

	public void Complete()
	{
		quest.Complete();
	}
}
