﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class QuestPoint : MonoBehaviour, IAction {

	//Reach point and do some action.
	#pragma warning disable

	[SerializeField]Animator info_panel;

	#pragma warning restore

	[System.NonSerialized] public Quest quest;

	bool isInside;

	Sprite pc_action, xbox_action, ps4_action;

	void Update()
	{
		if (isInside && quest != null) 
		{
			Action ();
		}
	}

	public void Action()
	{
		if(quest.id == QuestManager.current.id)
		{
			if(Controller.Action())
			{
				info_panel.SetTrigger ("HideInfo");
				quest.Complete ();

				Destroy (gameObject);
			}
		}
	}

	void OnTriggerEnter(Collider coll)
	{
		if(coll.gameObject.tag == "Player" && quest != null)
		{
			isInside = true;

			if(info_panel.GetComponent<Image> ().sprite == null)
			{
				info_panel.GetComponent<Image> ().sprite = Controller.ActionSprite ();
			}
		
			info_panel.SetTrigger ("ShowInfo");
		
		}
	}

	void OnTriggerExit(Collider coll)
	{
		if(coll.gameObject.tag == "Player" && quest != null && isInside)
		{
			isInside = false;

			info_panel.SetTrigger ("HideInfo");
		}
	}
}
