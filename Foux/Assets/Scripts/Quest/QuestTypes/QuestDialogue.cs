﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class QuestDialogue : MonoBehaviour {

	[System.NonSerialized] public Quest quest;

	Animator anim;
	Text text;

	public IEnumerator StartDialogue(int delay)
	{
		anim = GetComponent<Animator> ();
		text = GetComponentInChildren<Text> ();
		
		text.text = Translate.GetTranslation()[quest.id];

		anim.SetTrigger ("Show");

		//yield return new WaitForSeconds (delay);

		while(!Controller.Action()){
			yield return null;

		}

		anim.SetTrigger ("Hide");
	}

}
