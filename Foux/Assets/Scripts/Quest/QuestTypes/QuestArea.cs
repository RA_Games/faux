﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class QuestArea : MonoBehaviour {

	//Reach point and nothing more.

	[System.NonSerialized] public Quest quest;

	bool isInside;

	void OnTriggerEnter(Collider coll)
	{		
		if(coll.gameObject.tag == "Player" && quest != null)
		{
			quest.Complete ();
		}
	}
}
