﻿using UnityEngine;
using System.Collections;

public class DialogueQuestAnimator : StateMachineBehaviour {

	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
	{
		//When this animation finish, the current dialogue quest its finish.
		animator.GetComponent<QuestDialogue>().quest.Complete();
	}
}
