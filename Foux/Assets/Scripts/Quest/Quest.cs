﻿using System;
using UnityEngine;
using System.Collections;

[Serializable]
public class Quest {

	public string id;
	public string text;
	public bool isComplete;

	public QuestType questType;

	public GameObject target;

	public Quest(string desc)
	{
		text = desc;
	}

	public void Complete()
	{
		isComplete = true;
	}
}
	

