﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PuzzleDoor: MonoBehaviour{

	#pragma warning disable
	[SerializeField]Animator info_panel;
	#pragma warning restore

	Puzzle puzzle;

	bool isInside;
	bool isActive;

	void Start()
	{
		puzzle = GetComponentInChildren<Puzzle> ();
	}

	void Update()
	{
		if (isInside) 
		{
			Action ();
		}
	}

	public void Action()
	{
		if(Controller.Action() && !isActive)
		{
			isActive = true;
			info_panel.SetTrigger ("HideInfo");

			puzzle.OpenPuzzle ();
		}

	}

	void OnTriggerEnter(Collider coll)
	{
		if(coll.gameObject.tag == "Player")
		{
			isInside = true;

			if(info_panel.GetComponent<Image> ().sprite == null)
			{
				info_panel.GetComponent<Image> ().sprite = Controller.ActionSprite ();
			}

			info_panel.SetTrigger ("ShowInfo");

		}
	}

	void OnTriggerExit(Collider coll)
	{
		if(coll.gameObject.tag == "Player" && isInside)
		{
			isInside = false;

			if(!isActive)
			{
				info_panel.SetTrigger ("HideInfo");
			}

			isActive = false;
		}
	}
}

