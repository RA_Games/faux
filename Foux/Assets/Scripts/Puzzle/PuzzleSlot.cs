﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PuzzleSlot : MonoBehaviour, ISelectHandler, IDeselectHandler {

	public int index;
	public bool correctPiece;

	Image slot_image;
	Button slot_button;
	Animator anim;

	bool selected;
	bool isEmpty = true;

	void Awake () 
	{
		slot_image = GetComponent<Image> ();
		slot_button = GetComponent<Button> ();
		anim = GetComponent<Animator> ();
	}

	void Start()
	{
		CheckPiece ();
	}

	void Update(){
		RotatePiece ();
	}

	public void OnSelect (BaseEventData eventData) 
	{
		if(isEmpty)
		{
			slot_image.sprite = Puzzle.CurrentPiece();
			selected = true;
		}

		slot_image.color = Color.gray;
	}

	public void OnDeselect (BaseEventData data) 
	{
		if (isEmpty) 
		{
			slot_image.sprite = null;
			slot_image.color = new Color (1, 1, 1, 0);
		} else 
		{
			slot_image.color = Color.white;
		}

		selected = false;
	}

	public void Submit ()
	{
		if(isEmpty)
		{
			SetPiece ();	
		}else
		{
			RemovePiece ();
		}
	}

	void SetPiece()
	{
		isEmpty = false;
		slot_image.sprite = Puzzle.CurrentPiece();
		slot_image.color = Color.white;

		anim.SetBool ("place", true);

		CheckPiece ();

		Puzzle.ConsumePiece ();
	}

	void CheckPiece()
	{
		if(slot_image.sprite != null)
		{
			if(slot_image.sprite.name == "Puzzle_" + index)
			{
				correctPiece = true;
			}
		}
	}

	void RemovePiece()
	{
		Puzzle.GetBackPiece (slot_image.sprite);
		//slot_image.sprite = null;
		slot_image.color = new Color (1, 1, 1, 1);

		anim.SetBool ("place", false);

		isEmpty = true;
		correctPiece = false;
	}

	public void UpdateSlot()
	{
		if(selected)
		{
			slot_image.sprite = Puzzle.CurrentPiece();
		}
	}

	void RotatePiece()
	{
		if (Controller.Triggers () > 0)
		{
			print (Controller.Triggers ());
		}
		else if (Controller.Triggers () < 0)
		{
			print (Controller.Triggers ());
		}
	}
}
