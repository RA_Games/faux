﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Puzzle : MonoBehaviour, IAction {

	public Material ui_mat;
	public string puzzleName;

	static List <Sprite> remainPieces = new List<Sprite> ();

	static int currentPieceIndex;

	bool isOpen;
	bool complete;
	Transform container;


	static Puzzle instance;

	void Awake()
	{
		container = GetComponentInChildren<GridLayoutGroup> ().transform;
		instance = this;
	}

	void Start()
	{
		FindPuzzle ();
		OpenPuzzle ();
	}

	void Update()
	{
		ChanguePiece ();
	}

	public void Action()
	{
		OpenPuzzle ();
	}

	public static Sprite CurrentPiece()
	{
		return remainPieces[currentPieceIndex];
	}

	public static void ConsumePiece()
	{
		remainPieces.Remove (remainPieces[currentPieceIndex]);
		currentPieceIndex = 0;

		instance.CheckPuzzlePieces ();
	}

	public static void GetBackPiece(Sprite piece)
	{
		remainPieces.Insert (0,piece);
		currentPieceIndex = 0;
	}

	public void ChanguePiece()
	{
		if(Controller.Bumper() > 0)
		{
			if (currentPieceIndex < (remainPieces.Count - 1)) 
			{
				currentPieceIndex++;
			} else 
			{
				currentPieceIndex = 0;
			}

			foreach(Transform child in container)
			{
				child.GetComponent<PuzzleSlot> ().UpdateSlot ();
			}
		}

		if(Controller.Bumper() < 0)
		{
			if (currentPieceIndex > 0) 
			{
				currentPieceIndex--;
			} else 
			{
				currentPieceIndex = remainPieces.Count - 1;
			}

			foreach(Transform child in container)
			{
				child.GetComponent<PuzzleSlot> ().UpdateSlot ();
			}
		}
	}

	public void OpenPuzzle()
	{
		isOpen = !isOpen;

		StartCoroutine (Fade());

		Cursor.lockState = CursorLockMode.None;

		if(isOpen)
			InitializePieces ();

		PlayerMovement.StopingMovement ();
	}

	void CheckPuzzlePieces()
	{
		foreach(Transform child in container)
		{
			if(!child.GetComponent<PuzzleSlot>().correctPiece)
			{
				return;
			}
		}

		FinishPuzzle ();

	}

	void FinishPuzzle()
	{
		isOpen = !isOpen;

		StartCoroutine (Fade());

		complete = true;

		foreach(Transform child in container)
		{
			child.GetComponent<Button> ().interactable = false;
		}
	}

	void InitializePieces()
	{
		Transform firstAviableSlot = null;
		bool found = false;

		foreach(Transform child in container)
		{
			if(child.GetComponent<Image>().sprite != null)
			{
				foreach(Sprite piece in remainPieces)
				{
					if(child.GetComponent<Image>().sprite.name == piece.name)
					{
						remainPieces.Remove (piece);
						child.GetComponent<Button> ().interactable = false;
						child.GetComponent<Image> ().color = Color.white;
						break;
					}
				}
			}else
			{
				if(!found)
				{
					firstAviableSlot = child;
					found = true;
				}
			}
		}

		if(remainPieces.Count > 0)
		{
			currentPieceIndex = 0;
		}

		firstAviableSlot.GetComponent<Button> ().Select ();
	}

	void FindPuzzle()
	{
		Sprite[] pieces = Resources.LoadAll<Sprite> ("Puzzles/" + puzzleName);

		foreach(Sprite piece in pieces)
		{
			remainPieces.Add (piece);
		}

		for(int i = 0; i < remainPieces.Count; i++)
		{
			Sprite tmp = remainPieces [i];
			int r = Random.Range (i, remainPieces.Count);
			remainPieces [i] = remainPieces [r];
			remainPieces [r] = tmp;
		}

		int index = 0;

		foreach(Transform child in container)
		{
			child.GetComponent<PuzzleSlot> ().index = index;

			index++;
		}
	}

	IEnumerator Fade()
	{		
		if(isOpen)
		{
			while(ui_mat.color.a < 1)
			{
				ui_mat.color = new Color(ui_mat.color.r, ui_mat.color.g, ui_mat.color.b, ui_mat.color.a + (Time.deltaTime / 2));
				yield return new WaitForSeconds (Time.deltaTime);
			}

		}else
		{
			while(ui_mat.color.a > 0)
			{
				ui_mat.color = new Color(ui_mat.color.r, ui_mat.color.g, ui_mat.color.b, ui_mat.color.a - (Time.deltaTime / 2));
				yield return new WaitForSeconds (Time.deltaTime);
			}

			if(complete)
			{
				PlayerMovement.AllowMovement ();
				transform.parent.GetComponent<IDoor> ().OpenDoor ();
			}
		}

		yield return null;
	}

	void OnDisable()
	{
		ui_mat.color = new Color(ui_mat.color.r, ui_mat.color.g, ui_mat.color.b, 0);
	}
}
