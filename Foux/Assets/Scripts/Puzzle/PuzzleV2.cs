﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PuzzleV2 : MonoBehaviour {

	public static PuzzleV2 instance;

	public string puzzleName;

	[HideInInspector]
	public Image selectedSlot;
	public PuzzleSlotV2 selectedScript;
	public int pieceIndex, correctPieces;

	List <Material> remainPieces = new List<Material> ();

	int remainingPieces;
	MovieTexture movie;
	Image piecePreview;
	Material[] pieces;

	// Use this for initialization
	void Awake () {
		pieces = Resources.LoadAll<Material> ("Puzzles");
		movie = Resources.Load<MovieTexture> ("Puzzles/" + puzzleName + "/" + puzzleName);
		piecePreview = GetComponentInChildren<Image> ();
	}

	void Start () {
		instance = this;
		AssignPuzzle ();
		pieceIndex = Random.Range (0, pieces.Length);
		remainingPieces = 12;
	}

	void AssignPuzzle(){
		foreach(Material piece in pieces)
		{
			MovieTexture movieTexture = piece.mainTexture as MovieTexture;
			movieTexture.loop = true;
			movieTexture.Play ();
			piece.SetTexture (puzzleName, movie);
			remainPieces.Add (piece);
		}
		RandomizePuzzle ();
	}

	void RandomizePuzzle(){
		for(int i = 0; i < remainPieces.Count; i++)
		{
			Material tmp = remainPieces [i];
			int r = Random.Range (i, remainPieces.Count);
			remainPieces [i] = remainPieces [r];
			remainPieces [r] = tmp;
		}
	}

	public void UpdateSlot(){
		if(selectedScript.isEmpty){
			selectedSlot.material = remainPieces [pieceIndex];
		}
		piecePreview.material = remainPieces [pieceIndex];
	}

	void NextPiece(){
		if (pieceIndex == remainPieces.Count - 1) {
			pieceIndex = 0;
		} else {
			pieceIndex++;
		}
		if (remainPieces [pieceIndex] == null && remainingPieces > 0) {
			NextPiece ();
		} else {
			UpdateSlot ();
		}
	}

	void NextPieceWithoutUpdate(){
		if (pieceIndex == remainPieces.Count - 1) {
			pieceIndex = 0;
		} else {
			pieceIndex++;
		}
		if (remainPieces [pieceIndex] == null && remainingPieces > 0) {
			NextPieceWithoutUpdate ();
		}
	}

	void PreviousPiece(){
		if (pieceIndex == 0) {
			pieceIndex = remainPieces.Count - 1;
		} else {
			pieceIndex--;
		}
		if (remainPieces [pieceIndex] == null && remainingPieces > 0) {
			PreviousPiece ();
		} else {
			UpdateSlot ();
		}
	}

	public void ChangePiece()
	{
		if(Controller.Bumper() > 0)
		{
			NextPiece ();
		}

		if(Controller.Bumper() < 0)
		{
			PreviousPiece ();
		}
	}

	public void UpdateList(bool remove, int index, Material mat){
		if(remove){
			remainPieces[pieceIndex] = null;
			remainingPieces -= 1;
			if (remainingPieces > 0) {
				NextPieceWithoutUpdate ();
			} else if(correctPieces == 12) {
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////
				print("COMPLETE");
				//////////////////////////////////////////////////////////////////////////////////////////////////////////////
			}
		} else {
			remainingPieces += 1;
			remainPieces[index] = mat;
			pieceIndex = index;
		}
	}

	// Update is called once per frame
	void Update () {
		ChangePiece ();
	}
}
