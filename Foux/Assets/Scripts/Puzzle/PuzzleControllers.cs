﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PuzzleControllers : MonoBehaviour {

	public Image lBumper, rBumper, submit, back;

	void OnEnable()
	{
		UpdateSprites ();
	}

	void UpdateSprites()
	{
		lBumper.sprite = Controller.BumperSprite ("L");
		rBumper.sprite = Controller.BumperSprite ("R");

		submit.sprite =  Controller.ActionSprite ();
		back.sprite =  Controller.CancelSprite();
	}
}
