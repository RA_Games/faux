﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PuzzleSlotV2 : MonoBehaviour, ISelectHandler, IDeselectHandler {

	[HideInInspector]
	public bool isEmpty = true;

	int pieceIndex;
	Image myImage;
	GameObject selectSquare;
	Button btn;
	Animator anim;

	// Use this for initialization
	void Awake () {
		myImage = GetComponent<Image> ();
		btn = GetComponent<Button> ();
		anim = GetComponent<Animator> ();
		selectSquare = transform.GetChild (0).gameObject;
	}

	void Start(){
		if(gameObject.name == "PuzzleSlot_0"){
			btn.Select ();
		}
	}

	public void OnSelect (BaseEventData eventData) {
		PuzzleV2.instance.selectedSlot = myImage;
		PuzzleV2.instance.selectedScript = this;
		if(isEmpty){
			myImage.color = Color.white;
			PuzzleV2.instance.UpdateSlot ();
		}
		selectSquare.SetActive (true);
	}

	public void OnDeselect (BaseEventData eventData){
		if(isEmpty){
			myImage.color = Color.clear;
			myImage.material = null;
		}
		selectSquare.SetActive (false);
	}

	public void Submit(){
		if(isEmpty) {
			SetPiece ();
		} else {
			RemovePiece ();
		}
	}

	void SetPiece() {
		if(gameObject.name == myImage.material.name){
			PuzzleV2.instance.correctPieces ++;
		}
		pieceIndex = PuzzleV2.instance.pieceIndex;
		isEmpty = false;
		anim.SetBool ("place", true);
		PuzzleV2.instance.UpdateList (true, pieceIndex, null);
	}

	void RemovePiece() {
		if(gameObject.name == myImage.material.name){
			PuzzleV2.instance.correctPieces --;
		}
		anim.SetBool ("place", false);
		isEmpty = true;
		PuzzleV2.instance.UpdateList (false, pieceIndex, myImage.material);
	}
}
