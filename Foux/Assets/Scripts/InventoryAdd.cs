﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryAdd : MonoBehaviour {

	public GameObject inventory, miniStory;
	public int itemIndex;

	void OnTriggerStay(Collider other){
		if(other.gameObject.tag == "Player"){
			if (Controller.Action ()) {
				AddInventory ();
			}
		}
	}

	void AddInventory(){
		Time.timeScale = 0;
		miniStory.name = "Itm_" + itemIndex.ToString();
		Inventory.itemIndex = itemIndex;

		Inventory.isPaused = false;
		inventory.SetActive (true);
		Destroy (this.gameObject);
	}
}
