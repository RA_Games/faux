﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Pause : MonoBehaviour {

	public static string returnMenu, selectedBtn;

	public GameObject[] menus;
	public GameObject PauseText, ministory;

	[SerializeField]
	public Button resumeButton, puzzleSlot, audioBtn, resolution, controlA, english, inventoryFirst;
	public Slider audioVolume;

	// Use this for initialization
	void Start () {
		menus[0].SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		if (Controller.Esc ()) {
			PauseGame ();
		}
		if (Controller.Cancel () && Inventory.isPaused) {
			if(returnMenu == "pause"){
				DisableMenus ();
				resumeButton.Select ();
				PauseText.SetActive (true);
				returnMenu = "pause";
			} else if (returnMenu != "inventory"){
				DisableMenus ();
				audioBtn.Select ();
				menus[2].SetActive (true);
				returnMenu = "pause";
			}
		}
	}

	void PauseGame(){
		if (!menus[0].activeSelf) {
			Time.timeScale = 0;
			DisableMenus ();
			PauseText.SetActive (true);
			menus[0].SetActive (true);
			resumeButton.Select ();
			returnMenu = "pause";
			Inventory.isPaused = true;
		} else {
			Time.timeScale = 1;
			puzzleSlot.Select ();
			menus[0].SetActive (false);
			Inventory.isPaused = false;
		}
	}

	public void MenuSelection(string selection){
		switch(selection){
		case "resume":
			PauseGame ();
			break;
		case "inventory":
			DisableMenus ();
			menus [1].SetActive (true);
			inventoryFirst.Select ();
			returnMenu = "pause";
			break;
		case "options":
			audioBtn.Select ();
			DisableMenus ();
			menus[2].SetActive (true);
			returnMenu = "pause";
			break;
		case "exit":
			PauseGame ();
			break;
		case "audio":
			DisableMenus ();
			audioVolume.Select ();
			menus[3].SetActive (true);
			returnMenu = "options";
			break;
		case "video":
			DisableMenus ();
			resolution.Select ();
			menus[4].SetActive (true);
			returnMenu = "options";
			break;
		case "controller":
			DisableMenus ();
			controlA.Select ();
			menus[5].SetActive (true);
			returnMenu = "options";
			break;
		case "language":
			DisableMenus ();
			english.Select ();
			menus[6].SetActive (true);
			returnMenu = "options";
			break;
		case "ministory":
			selectedBtn = EventSystem.current.currentSelectedGameObject.name;
			ministory.name = selectedBtn;
			menus[7].SetActive (true);
			returnMenu = "inventory";
			break;
		}
	}

	public void DisableMenus(){
		PauseText.SetActive (false);
		for(int i = 1; i < menus.Length; i++){
			menus[i].SetActive (false);
		}
	}
}
