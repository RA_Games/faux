﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class LightBroken : MonoBehaviour {

	[Range(3,0.5f)]
	public float maxClosePoint = 0.5f;

	#pragma warning disable 
	[SerializeField]Material light_ON;
	[SerializeField]Material light_OFF;
	#pragma warning restore

	Transform player;
	Light light_component;
	Renderer bulb_renderer;
	ParticleSystem[] sparks;

	float delta;

	void Start()
	{
		light_component = transform.GetComponentInChildren<Light> ();
		bulb_renderer = transform.FindChild ("Bulb").GetComponent<Renderer> ();
		sparks = transform.GetComponentsInChildren<ParticleSystem> ();
	}

	void OnTriggerEnter(Collider coll)
	{
		if (coll.gameObject.tag == "Player") 
		{			
			StartCoroutine (LightFailure());
		}
	}

	void OnTriggerStay(Collider coll)
	{
		if(coll.gameObject.tag == "Player")
		{
			player = coll.transform;

			delta = Vector2.Distance (new Vector2(player.position.x,0), new Vector2(transform.position.x,0));
		}
	}

	void OnTriggerExit(Collider coll)
	{		
		if (coll.gameObject.tag == "Player") 
		{
			if(delta < maxClosePoint)
			{
				ToggleLight (false);

				sparks[0].Play ();
				sparks[1].Play ();

				Destroy (this);
			}else
			{
				StopCoroutine (LightFailure());
			}
		}
	}

	IEnumerator LightFailure()
	{
		yield return new WaitForSeconds (Time.deltaTime);
		
		while(delta > maxClosePoint)
		{
			ToggleLight (false);

			yield return new WaitForSeconds (Time.deltaTime * delta * Random.Range(1,10));

			ToggleLight (true);

			yield return new WaitForSeconds (Time.deltaTime * delta * Random.Range(1,10));

			yield return null;
		}
	}

	/// <summary>
	/// Toggles the ligh.
	/// </summary>
	/// <param name="value">Value.</param>
	void ToggleLight(bool value)
	{
		light_component.enabled = value;

		if(value)
		{
			bulb_renderer.material = light_ON;
		}else
		{
			bulb_renderer.material = light_OFF;
		}
	}
}
