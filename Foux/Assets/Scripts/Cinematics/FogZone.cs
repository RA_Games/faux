﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class FogZone : MonoBehaviour {

	[Range(3,0.5f)]
	public float maxClosePoint = 0.5f;

	Transform player;

	float delta;
	float currentDelta;

	protected float fogMinimun;
	protected float fogMax;

	void Start()
	{
		fogMinimun = 5;
		fogMax = 45;
		currentDelta = 100;
	}

	void OnTriggerEnter(Collider coll)
	{
		if (coll.gameObject.tag == "Player") 
		{
			StopCoroutine (RemoveFog());
			player = coll.transform;
		}
	}

	void OnTriggerStay(Collider coll)
	{
		if(coll.gameObject.tag == "Player")
		{	
			delta = Vector2.Distance (new Vector2(player.position.x,0), new Vector2(transform.position.x,0));

			if(delta < currentDelta)
			{
				currentDelta = delta;

				if(RenderSettings.fogEndDistance > fogMinimun)
				{
					RenderSettings.fogEndDistance = Mathf.Lerp (RenderSettings.fogEndDistance, fogMinimun, delta * 0.001f);
				}
			}
		}
	}

	void OnTriggerExit(Collider coll)
	{		
		if (coll.gameObject.tag == "Player") 
		{
			currentDelta = 100;
			StartCoroutine (RemoveFog());
		}
	}

	IEnumerator RemoveFog()
	{		
		while (RenderSettings.fogEndDistance < fogMax )
		{
			RenderSettings.fogEndDistance += 0.5f;

			yield return new WaitForSeconds (Time.deltaTime);
		}

		RenderSettings.fogEndDistance = fogMax;
	}
}