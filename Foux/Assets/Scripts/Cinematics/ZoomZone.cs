﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(SphereCollider))]
public class ZoomZone : MonoBehaviour {

	[Range(0, 10)]
	public float offset = 0;

	[Range(1, 10)]
	public float height = 1;

	[Range(1, 10)]
	public float distance = 1;

	Vector3 colliderCenter;

	float closeCenter = 0.3f;
	float currentDelta = 100;

	SphereCollider collider_component;

	bool isUsed;

	void Start()
	{
		collider_component = GetComponent<SphereCollider> ();
		colliderCenter = collider_component.bounds.center;
	}

	void OnTriggerStay(Collider coll)
	{
		if (coll.tag == "Player" && !isUsed) 
		{
			float delta_Raw = Vector2.Distance (new Vector2 (colliderCenter.x, 0), new Vector2 (coll.transform.position.x, 0));
			float delta = ((delta_Raw + closeCenter) * 100) / collider_component.radius;

			if(delta < currentDelta)
			{
				currentDelta = delta;

				offset /= 100;
				float currentHeight = (height * delta / 100);
				float currentDistance = (distance * delta / 100);

				CameraMovement.SetZoom (new Vector3(offset, currentHeight, -currentDistance));

				if(delta < 2)
				{
					StartCoroutine (CameraMovement.ResetTarget (5));
					isUsed = true;
				}
			}
		}
	}

	void OnTriggerExit(Collider coll)
	{
		if(coll.tag == "Player" && isUsed)
		{
			Destroy (this);
		}
	}
}
