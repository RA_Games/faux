﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider))]
public class FocusZone : MonoBehaviour {

	[Tooltip("Re-usable?")]
	public bool isOneUse = false;

	[Range(0,10)]
	[Tooltip("Destroy delay when reach the target?")]
	public float reachDestroy = 0; 

	private float reachValue = 0.3f;
	private bool stop;

	void OnTriggerStay(Collider coll)
	{
		if(coll.tag == "Player")
		{			
			var d = Vector3.Distance(new Vector3(transform.position.x,0,0), new Vector3(Camera.main.transform.position.x,0,0));

			if(!stop)
			{
				CameraMovement.SetTarget (transform.position);
			}

			if(reachDestroy > 0)
			{
				if(d < reachValue && !stop)
				{
					stop = true;
					StartCoroutine (Resseting());
				}
			}
		} 
	}

	void OnTriggerExit(Collider coll)
	{
		if(coll.tag == "Player" && reachDestroy == 0)
		{
			StartCoroutine (CameraMovement.ResetTarget (0));

			if(isOneUse)
			{
				Destroy (gameObject);
			}
		} 
	}

	IEnumerator Resseting()
	{
		yield return StartCoroutine (CameraMovement.ResetTarget (reachDestroy));
		Destroy (gameObject);
	}
		
}
