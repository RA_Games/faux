﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class Door : MonoBehaviour, IDoor {

	public bool isOpen;

	Animator anim;

	void Start () 
	{
		anim = gameObject.GetComponent<Animator> ();

		if(isOpen)
		{
			anim.SetTrigger ("Close");
		}
	}

	public IEnumerator OpenDoor () 
	{
		if(isOpen)
		{
			anim.SetTrigger ("Close");
			isOpen = false;
			yield return new WaitForSeconds (1);
		}else
		{
			anim.SetTrigger ("Open");
			isOpen = true;
			yield return new WaitForSeconds (1);
		}

	}
}
