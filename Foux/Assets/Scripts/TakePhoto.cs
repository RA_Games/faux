﻿using UnityEngine;
using System.Collections;
using System.IO;

public class TakePhoto : MonoBehaviour {

	public KeyCode photo_key = KeyCode.P;
	public KeyCode rec_key = KeyCode.R;

	int frame;
	bool rec;

	void Update () 
	{
		if(Input.GetKeyDown(photo_key))
		{
			Application.CaptureScreenshot ("Screenshots/Screenshot_" + frame + ".png",4);
			print ("New Screenshot");
			frame++;
		}

		if(Input.GetKeyDown(rec_key))
		{
			rec = !rec;
		}

		if (rec)
		{
			Application.CaptureScreenshot ("Video/Frame" + frame + ".png");
			Time.timeScale = 0.1f;
			frame++;
		} else 
		{
			Time.timeScale = 1;
		}
	}
}
