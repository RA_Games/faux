﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItem {

	public string name;
	public Sprite sprite;
	public bool isUnlocked;
	public int index;
}
