﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Settings: MonoBehaviour {

	public static bool isPaused;

	public enum idioms
	{
		Esp,
		Eng,
		Jap,
	}

	public Dropdown language_settigns;
	public Dropdown resolution_settigns;

	public Slider music_setting;
	public Slider sfx_setting;

	public static bool gamepad = false;

	private idioms currentLang;
	private int currentRes;
	private float currentMusic, currentSFX;

	public void LoadSettings()
	{
		currentLang = GameData.data.laguage;
		currentRes = GameData.data.resValue;
		currentMusic = GameData.data.musicVolume;
		currentSFX = GameData.data.sfxVolume;

		switch(GameData.data.laguage)
		{
		case idioms.Eng:
			language_settigns.value = 0;
			break;

		case idioms.Esp:
			language_settigns.value = 1;
			break;

		case idioms.Jap:
			language_settigns.value = 2;
			break;
		}

		resolution_settigns.value = currentRes;

		music_setting.value = currentMusic;
		sfx_setting.value = currentSFX;			
	}

	public static idioms GetLanguage () 
	{
		return GameData.data.laguage;
	}

	public void SetMusic()
	{
		GameData.data.musicVolume = music_setting.value;
	}

	public void SetSFX()
	{
		GameData.data.sfxVolume = sfx_setting.value;
	}

	public void SetResolution()
	{
		string[] rawRes = resolution_settigns.options [resolution_settigns.value].text.Split('x');
		int x = int.Parse (rawRes[0]);
		int y = int.Parse (rawRes[1]);

		GameData.data.resX = x;
		GameData.data.resY = y;
		GameData.data.resValue = resolution_settigns.value;
	}

	public void SetLanguage()
	{
		switch(language_settigns.value)
		{
		case 0:
			GameData.data.laguage = idioms.Eng;
			break;

		case 1:
			GameData.data.laguage = idioms.Esp;
			break;

		case 2:
			GameData.data.laguage = idioms.Jap;
			break;
		}
	}

	public void Apply()
	{
		if(currentLang != GameData.data.laguage)
		{
			TranslateUI.TranslateAll ();
			currentLang = GameData.data.laguage;
			GameData.Save ();
		}

		if(currentRes != GameData.data.resValue)
		{
			Screen.SetResolution (GameData.data.resX, GameData.data.resY, true, 1);
			currentRes = GameData.data.resValue;
			GameData.Save ();
		}

		if(currentMusic != GameData.data.musicVolume)
		{
			currentMusic = GameData.data.musicVolume;
			GameData.Save ();
		}

		if(currentSFX != GameData.data.sfxVolume)
		{
			currentSFX = GameData.data.sfxVolume;
			GameData.Save ();
		}
	}
}
