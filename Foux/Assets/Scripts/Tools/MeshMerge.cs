﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class MeshMerge : MonoBehaviour {

	public void Merge()
	{
		Quaternion oldRot = transform.rotation;
		Vector3 oldPos = transform.position;
		
		MeshFilter[] meshes = GetComponentsInChildren<MeshFilter> ();
		CombineInstance[] combine = new CombineInstance[meshes.Length];
		MeshRenderer renderer = transform.GetChild (0).GetComponent<MeshRenderer>();

		int i = 0;

		while (i < meshes.Length) 
		{
			combine[i].mesh = meshes[i].sharedMesh;
			combine[i].transform = meshes[i].transform.localToWorldMatrix;
			meshes[i].gameObject.SetActive(false);
			i++;
		}

		transform.GetComponent<MeshFilter>().mesh = new Mesh();
		transform.GetComponent<MeshFilter>().sharedMesh.CombineMeshes(combine,true,true);
		transform.gameObject.SetActive(true);
		GetComponent<Renderer> ().material = renderer.sharedMaterial;

		transform.rotation = oldRot;
		transform.position = oldPos;

		DestroyImmediate (this);
	}

}
