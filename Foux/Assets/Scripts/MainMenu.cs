﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainMenu : MonoBehaviour {
	
	public Button first_main;
	public Dropdown first_settings;

	Animator anim;

	Vector3 lastMousePos;

	bool usingNavigation = true;

	enum panels{MainMenu,Settings}
	panels currentPanel = panels.MainMenu;

	void Start () 
	{
		anim = Camera.main.GetComponent<Animator> ();
		Cursor.visible = false;
		first_main.Select ();
	}

	void Update()
	{
		if(Input.mousePosition != lastMousePos)
		{
			usingNavigation = false;
			Cursor.visible = true;

			EventSystem.current.GetComponent<EventSystem>().SetSelectedGameObject(null);
			Selectable[] allUI = GameObject.FindObjectsOfType (typeof(Selectable)) as Selectable[];
			Navigation customNav = new Navigation ();
			customNav.mode = Navigation.Mode.None;

			foreach(Selectable ui in allUI)
			{
				ui.navigation = customNav;
			}

			lastMousePos = Input.mousePosition;
		}

		if((Mathf.Abs(Input.GetAxis("Horizontal")) > 0.1f || Mathf.Abs(Input.GetAxis("Vertical")) > 0.1f) && usingNavigation == false)
		{
			usingNavigation = true;
			Cursor.visible = false;

			Selectable[] allUI = GameObject.FindObjectsOfType (typeof(Selectable)) as Selectable[];
			Navigation customNav = new Navigation ();
			customNav.mode = Navigation.Mode.Automatic;

			foreach(Selectable ui in allUI)
			{
				ui.navigation = customNav;
			}

			switch(currentPanel)
			{
			case panels.MainMenu:
				first_main.Select ();
				break;
			}
		}
	}

	public void ShowOptions () 
	{
		currentPanel = panels.Settings;
		anim.SetTrigger ("Settings");
		first_settings.Select ();
	}

	public void HideOptions () 
	{
		currentPanel = panels.MainMenu;
		anim.SetTrigger ("MainMenu");
		first_main.Select ();
	}
}
