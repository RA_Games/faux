﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TranslateUI : MonoBehaviour {

	public List<string> options = new List<string> ();

	Text text;
	Dropdown drop;

	void Start () 
	{
		ComponentGet ();
	}

	void OnEnable()
	{
		ComponentGet ();
	}

	public void ComponentGet()
	{
		if(GetComponent<Text>())
		{
			text = GetComponent<Text> ();
		}

		if(GetComponent<Dropdown>())
		{
			drop = GetComponent<Dropdown>();	
		}

		TranslateText ();
	}

	void TranslateText () 
	{	
		if(GetComponent<Text>())
		{
			text.text = Translate.GetTranslation () [gameObject.name];
		}

		if(GetComponent<Dropdown>())
		{
			for(int i = 0; i < options.Count; i++)
			{
				drop.options[i].text = Translate.GetTranslation()[options[i]];
			}

			drop.RefreshShownValue ();
		}
	}

	public static void TranslateAll()
	{		
		TranslateUI[] text_components = GameObject.FindObjectsOfType (typeof(TranslateUI)) as TranslateUI[];

		foreach(TranslateUI text in text_components)
		{
			text.TranslateText();
		}
	}
}
