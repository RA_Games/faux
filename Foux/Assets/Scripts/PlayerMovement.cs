﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour {

	public float speed;

	CharacterController cc;
	Animator anim;

	Vector3 moveDir = Vector3.zero;
	Vector3 velocity;
	Vector3 lasDirectionMove;

	static bool canMove = true;


	void Start ()
	{
		cc = GetComponent<CharacterController> ();
		cc.detectCollisions = true;
		anim = GetComponent<Animator> ();
	}

	void FixedUpdate () 
	{
		if(canMove)
		{
			var h = Vector3.right * Input.GetAxis ("Horizontal");
			var v = Vector3.forward * Input.GetAxis ("Vertical");

			moveDir = h + v;

			if(moveDir == lasDirectionMove)
			{
				moveDir = lasDirectionMove;
			}

		
			cc.Move (moveDir * Time.deltaTime * speed);

			velocity = cc.velocity;
			velocity.x = Mathf.Clamp (cc.velocity.x, -1, 1);
			velocity.z = Mathf.Clamp (cc.velocity.z, -1, 1);

			if (Input.GetAxis ("Horizontal") > 0) 
			{
				lasDirectionMove = Vector3.Lerp (lasDirectionMove, new Vector3 (1, 0, 0), cc.velocity.magnitude);
			} else if(Input.GetAxis ("Horizontal") < 0)
			{
				lasDirectionMove = Vector3.Lerp (lasDirectionMove, new Vector3 (-1, 0, 0), cc.velocity.magnitude);
			}else if (Input.GetAxis ("Vertical") > 0) 
			{
				lasDirectionMove = Vector3.Lerp (lasDirectionMove, new Vector3 (0, 0, 1), cc.velocity.magnitude);
			} else 
			{
				lasDirectionMove = Vector3.Lerp (lasDirectionMove, new Vector3 (0, 0, -1), cc.velocity.magnitude);
			}
		}

		anim.SetFloat ("Horizontal",lasDirectionMove.x);
		anim.SetFloat ("Vertical",lasDirectionMove.z);
		anim.SetFloat ("Speed",cc.velocity.magnitude);
	}

	public static void StopingMovement()
	{
		canMove = false;
	}

	public static void AllowMovement()
	{
		canMove = true;
	}
}
