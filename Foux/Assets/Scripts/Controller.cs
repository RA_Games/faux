﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Controller: MonoBehaviour{

	public bool mainMenu;

	StandaloneInputModule input;

	static Sprite pc_action, xbox_action, ps4_action, pc_LBumber, pc_RBumper, xbox_LBumper, xbox_RBumper, ps4_LBumper, ps4_RBumper, pc_Cancel, xbox_cancel, ps4_cancel;

	void Awake()
	{	
		DisableCursor ();	
		
		if(mainMenu)
		{
			if(!Settings.gamepad)
			{
				EnableCursor ();
			}
		}

		LoadSprites ();
	}

	void Start()
	{
		input = GetComponent<StandaloneInputModule> ();

		UpdateInputStandalone ();
	}
		
	public static Sprite BumperSprite(string side)
	{
		#if UNITY_EDITOR_WIN

		if(Settings.gamepad)
		{
			if(side == "L")
			{
				return xbox_LBumper;
			}

			if(side == "R")
			{
				return xbox_RBumper;
			}

			return null;
				
		}else
		{
			if(side == "L")
			{
				return pc_LBumber;
			}

			if(side == "R")
			{
				return pc_RBumper;
			}

			return null;
		}
		#endif

		#if UNITY_XBOXONE

		if(side == "L")
		{
			return xbox_LBumper;
		}

		if(side == "R")
		{
			return xbox_RBumper;
		}

		return null;

		#endif

		#if UNITY_PS4

		if(side == "L")
		{
			return ps4_LBumper;
		}

		if(side == "R")
		{
			return ps4_RBumper;
		}

		return null;

		#endif

		#pragma warning disable
		return null;
		#pragma warning restore
	}

	public static int Bumper()
	{
		#if UNITY_EDITOR_WIN

		if(Settings.gamepad)
		{
			if(Input.GetKeyDown(KeyCode.JoystickButton4))
			{
				return -1;
			}

			if(Input.GetKeyDown(KeyCode.JoystickButton5))
			{
				return 1;
			}
		}else
		{
			if(Input.GetKeyDown(KeyCode.C))
			{
				return -1;
			}

			if(Input.GetKeyDown(KeyCode.V))
			{
				return 1;
			}
		}

		return 0;

		#endif

		#if UNITY_XBOXONE

		if(Input.GetKey(KeyCode.JoystickButton4))
		{
		return -1;
		}

		if(Input.GetKey(KeyCode.JoystickButton5))
		{
		return 1;
		}

		#endif

		#if UNITY_PS4

		if(Input.GetKey(KeyCode.JoystickButton4))
		{
		return -1;
		}

		if(Input.GetKey(KeyCode.JoystickButton5))
		{
		return 1;
		}

		#endif

		#pragma warning disable
		return 0;
		#pragma warning restore
	}

	public static int Triggers()
	{
		#if UNITY_EDITOR_WIN

		if(Settings.gamepad)
		{
			if(Input.GetAxis("TriggersPC") < 0)
			{
				return -1;
			}

			if(Input.GetAxis("TriggersPC") > 0)
			{
				return 1;
			}
		}
		else
		{
			if(Input.GetKeyDown(KeyCode.B))
			{
				return -1;
			}

			if(Input.GetKeyDown(KeyCode.N))
			{
				return 1;
			}
		}

		return 0;

		#endif

		#if UNITY_XBOXONE

		if(Input.GetAxis("TriggersPC") < 0)
		{
		return -1;
		}

		if(Input.GetAxis("TriggersPC") > 0)
		{
		return 1;
		}

		#endif

		#if UNITY_PS4

		if(Input.GetAxis("LTriggerPS4") > -1)
		{
		return -1;
		}

		if(Input.GetAxis("RTriggerPS4") > -1)
		{
		return 1;
		}

		#endif

		#pragma warning disable
		return 0;
		#pragma warning restore
	}

	public static bool Action()
	{
		#if UNITY_EDITOR_WIN

		if(Settings.gamepad)
		{
			return Input.GetKeyDown(KeyCode.JoystickButton0);
		}else
		{
			return Input.GetKeyDown(KeyCode.Z);
		}

		#endif

		#if UNITY_XBOXONE

		return Input.GetKey(KeyCode.JoystickButton0);

		#endif

		#if UNITY_PS4

		return Input.GetKey(KeyCode.JoystickButton1);

		#endif

		#pragma warning disable
		return false;
		#pragma warning restore
	}

	public static bool Esc()
	{
		#if UNITY_EDITOR_WIN

		if(Settings.gamepad)
		{
			return Input.GetKeyDown(KeyCode.JoystickButton7);
		}else
		{
			return Input.GetKeyDown(KeyCode.Escape);
		}

		#endif

		#if UNITY_XBOXONE

		return Input.GetKey(KeyCode.JoystickButton7);

		#endif

		#if UNITY_PS4

		return Input.GetKey(KeyCode.JoystickButton9);

		#endif

		#pragma warning disable
		return false;
		#pragma warning restore
	}

	void UpdateInputStandalone ()
	{
		#if UNITY_EDITOR_WIN

		if(Settings.gamepad)
		{
			input.submitButton = "ActionXbox";
			input.cancelButton = "CancelXbox";
		}else
		{
			input.submitButton = "ActionPc";
			input.cancelButton = "CancelPc";
		}

		#endif

		#if UNITY_XBOXONE

		input.submitButton = "ActionXbox";
		input.cancelButton = "CancelXbox";

		#endif

		#if UNITY_PS4

		input.submitButton = "ActionPS4";
		input.cancelButton = "CancelPS4";

		#endif
	}

	public static Sprite ActionSprite()
	{
		#if UNITY_EDITOR_WIN

		if(Settings.gamepad)
		{
			return xbox_action;
		}else
		{
			return pc_action;
		}

		#endif

		#if UNITY_XBOXONE

		return xbox_action;

		#endif

		#if UNITY_PS4

		return ps4_action;

		#endif

		#pragma warning disable
		return null;
		#pragma warning restore
	}

	public static bool Cancel()
	{
		#if UNITY_EDITOR_WIN

		if(Settings.gamepad)
		{
			return Input.GetKeyDown(KeyCode.JoystickButton1);
		}else
		{
			//return Input.GetKeyDown(KeyCode.Escape);
			return Input.GetKeyDown(KeyCode.X);
		}

		#endif

		#if UNITY_XBOXONE

		return Input.GetKey(KeyCode.JoystickButton1);

		#endif

		#if UNITY_PS4

		return Input.GetKey(KeyCode.JoystickButton2);

		#endif

		#pragma warning disable
		return false;
		#pragma warning restore
	}

	public static Sprite CancelSprite()
	{
		#if UNITY_EDITOR_WIN

		if(Settings.gamepad)
		{
			return xbox_cancel;
		}else
		{
			return pc_Cancel;
		}

		#endif

		#if UNITY_XBOXONE

		return xbox_cancel;

		#endif

		#if UNITY_PS4

		return ps4_cancel;

		#endif

		#pragma warning disable
		return null;
		#pragma warning restore
	}

	void LoadSprites()
	{
		foreach(Sprite button in Resources.LoadAll<Sprite>("Buttons"))
		{
			if(button.name == "PC_Action")
			{
				pc_action = button;
			}

			if(button.name == "Xbox_Action")
			{
				xbox_action = button;
			}

			if(button.name == "PS4_Action")
			{
				ps4_action = button;
			}

			if(button.name == "PC_Cancel")
			{
				pc_Cancel = button;
			}

			if(button.name == "Xbox_Cancel")
			{
				xbox_cancel = button;
			}

			if(button.name == "PS4_Cancel")
			{
				ps4_cancel = button;
			}

			if(button.name == "PC_LBumper")
			{
				pc_LBumber = button;
			}

			if(button.name == "PC_RBumper")
			{
				pc_RBumper = button;
			}

			if(button.name == "Xbox_LBumper")
			{
				xbox_LBumper = button;
			}

			if(button.name == "Xbox_RBumper")
			{
				xbox_RBumper = button;
			}

			if(button.name == "PS4_LBumper")
			{
				ps4_LBumper = button;
			}

			if(button.name == "PS4_RBumper")
			{
				ps4_RBumper = button;
			}
		}
	}

	public static void DisableCursor()
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	public static void EnableCursor()
	{
		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}
}
