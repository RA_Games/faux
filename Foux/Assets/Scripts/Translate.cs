﻿using UnityEngine;
using System.Collections;
using SimpleJSON;

public class Translate {

	public static JSONNode GetTranslation () 
	{			
		switch(GameData.data.laguage)
		{
		case Settings.idioms.Esp:
			return JSON.Parse (Resources.Load ("Translate/esp").ToString ());
		case Settings.idioms.Eng:
			return JSON.Parse (Resources.Load("Translate/eng").ToString());
		case Settings.idioms.Jap:
			break;
		}

		return null;
	}

}
