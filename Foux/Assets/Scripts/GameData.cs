﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 
using System.Runtime.Serialization.Formatters.Binary; 
using System.IO;

public class GameData : MonoBehaviour {

	public static Data data = new Data();

	private Settings settigns;

	void Awake()
	{
		settigns = GameObject.FindObjectOfType (typeof(Settings)) as Settings;
	}

	void Start () 
	{
		Load ();

		settigns.LoadSettings ();
	}

	public static void Save()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/Saves.data");
		bf.Serialize (file, data);
		file.Close ();
	}

	void Load()
	{
		if(File.Exists(Application.persistentDataPath + "/Saves.data"))
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/Saves.data", FileMode.Open);
			data = (Data)bf.Deserialize (file);
			file.Close ();
		}
	}

}
