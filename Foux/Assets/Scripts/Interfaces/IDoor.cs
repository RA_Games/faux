﻿using UnityEngine;
using System.Collections;

public interface IDoor {

	IEnumerator OpenDoor ();

}
