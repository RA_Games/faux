﻿using UnityEngine;
using System.Collections;

public interface IEvent {

	IEnumerator RunEvent();
}
