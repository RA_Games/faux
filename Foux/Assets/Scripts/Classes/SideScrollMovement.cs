﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideScrollMovement: IMovement {

	public void Move(Rigidbody rb, float speed)
	{
		rb.velocity = new Vector3 (Input.GetAxis ("Horizontal") * speed, rb.velocity.y, Input.GetAxis ("Vertical") * speed);

	
	}
}
