﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Data  {

	public Settings.idioms laguage = Settings.idioms.Eng;

	public int resX, resY, resValue = 8;

	public float musicVolume = 1, sfxVolume = 1;

	public List<bool> itemsActivated = new List<bool> ();
}
