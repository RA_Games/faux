﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

[CustomEditor(typeof(QuestManager))]
public class QuestManagerEditor : Editor {

	private ReorderableList list;

	private void OnEnable() 
	{
		list = new ReorderableList(serializedObject, serializedObject.FindProperty("quests"), true, true, true, true);

		list.drawElementCallback =  
			(Rect rect, int index, bool isActive, bool isFocused) => {
			var element = list.serializedProperty.GetArrayElementAtIndex(index);
			rect.y += 2;

			EditorGUI.LabelField(rect,"Quest");

			EditorGUI.PropertyField(
				new Rect(rect.x, rect.y, 60, EditorGUIUtility.singleLineHeight),
				element.FindPropertyRelative("questType").FindPropertyRelative("questType"), GUIContent.none);

			switch(element.FindPropertyRelative("questType").FindPropertyRelative("questType").enumNames[element.FindPropertyRelative("questType").FindPropertyRelative("questType").enumValueIndex])
			{
			case "Dialogue":
				EditorGUI.PropertyField(
					new Rect(rect.x + 65, rect.y, rect.width - 60 - 80, EditorGUIUtility.singleLineHeight),
					element.FindPropertyRelative("questType").FindPropertyRelative("qDialogue"), GUIContent.none);
				EditorGUI.PropertyField(
					new Rect(rect.x + rect.width - 75, rect.y, 50, EditorGUIUtility.singleLineHeight),
					element.FindPropertyRelative("id"), GUIContent.none);
				break;
			case "Point":
				EditorGUI.PropertyField(
					new Rect(rect.x + 65, rect.y, rect.width - 60 - 80, EditorGUIUtility.singleLineHeight),
					element.FindPropertyRelative("questType").FindPropertyRelative("qPoint"), GUIContent.none);
				break;
			case "Event":
				EditorGUI.PropertyField(
					new Rect(rect.x + 65, rect.y, rect.width - 60 - 80, EditorGUIUtility.singleLineHeight),
					element.FindPropertyRelative("questType").FindPropertyRelative("qEvent"), GUIContent.none);
				break;
			case "Area":
				EditorGUI.PropertyField(
					new Rect(rect.x + 65, rect.y, rect.width - 60 - 80, EditorGUIUtility.singleLineHeight),
					element.FindPropertyRelative("questType").FindPropertyRelative("qArea"), GUIContent.none);
				break;
			}
				
			EditorGUI.PropertyField(
				new Rect(rect.x + rect.width - 18 , rect.y, 30, EditorGUIUtility.singleLineHeight),
				element.FindPropertyRelative("isComplete"), GUIContent.none);
			
		};
	}

	public override void OnInspectorGUI() 
	{
		serializedObject.Update();
		list.DoLayoutList();
		serializedObject.ApplyModifiedProperties();
	}

}
