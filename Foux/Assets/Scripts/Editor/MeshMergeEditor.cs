﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(MeshMerge))]
public class MeshMergeEditor : Editor {

	public override void OnInspectorGUI() 
	{
		DrawDefaultInspector ();

		MeshMerge script = (MeshMerge)target;

		if(GUILayout.Button("Merge"))
		{
			script.Merge();
		}
	}
		
}
