﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class NeonFail : MonoBehaviour {

	Animator anim;
	public int seconds;

	void Start ()
	{
		anim = GetComponent<Animator> ();

		StartCoroutine (Fail());
	}

	IEnumerator Fail()
	{
		yield return new WaitForSeconds (seconds);

		anim.SetTrigger ("Fail");
	}
}
