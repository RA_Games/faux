﻿using UnityEngine;
using System.Collections;

public class Teaser : MonoBehaviour {

	public Rigidbody car;
	public Camera cam;
	public Transform target;

	public GameObject panel;

	bool  lockCamera = true;

	float camspeed;

	IEnumerator Start()
	{
		yield return new WaitForSeconds (12);

		Destroy (car.gameObject);
		lockCamera = false;

		StartCoroutine (AddCamSpeed());
		StartCoroutine (OnPanel());
	}

	void Update()
	{
		if (!lockCamera) 
		{			
			cam.transform.position = Vector3.Lerp (cam.transform.position, new Vector3 (target.position.x, 2.16f, -10), camspeed * Time.deltaTime);
		} else 
		{
			car.AddRelativeForce (Vector3.down * 8);
		}
	}

	IEnumerator AddCamSpeed()
	{
		float final = 0.3f;

		while(camspeed < final)
		{
			camspeed += 0.0001f;

			yield return new WaitForSeconds (Time.deltaTime);
		}
	}

	IEnumerator OnPanel()
	{
		yield return new WaitForSeconds (70);

		panel.SetActive (true);
	}
}
