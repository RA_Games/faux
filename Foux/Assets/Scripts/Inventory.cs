﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Inventory : MonoBehaviour {

	public static bool isPaused;

	public GameObject transparentBckg, inventoryMenu_Container;
	public Image objectImage;

	List <Sprite> inventoryImages = new List<Sprite> ();

	Button background;
	Button[] inventoryButtons;

	bool viewText;
	public static int itemIndex;
	string selectedBtn;

	// Use this for initialization
	void Awake () {
		background = GetComponentInChildren<Button> ();
		inventoryButtons = new Button[15];
		inventoryButtons = inventoryMenu_Container.GetComponentsInChildren<Button> ();
		LoadSprites ();
	}

	void Start () {
		
	}

	void OnEnable(){
		if(isPaused){
			selectedBtn = Pause.selectedBtn;
			itemIndex = Int32.Parse(selectedBtn.Substring (selectedBtn.Length - 1));
		}
		objectImage.sprite = inventoryImages[itemIndex];
		background.Select ();
	}

	// Update is called once per frame
	void Update () {
		if (Controller.Esc ()) {
			gameObject.SetActive (false);
			viewText = false;
			transparentBckg.SetActive (false);
		}
		if (Controller.Cancel ()) {
			if(viewText){
				transparentBckg.SetActive (false);
				viewText = false;
			} else {
				if (isPaused) {
					SelectLastSelected ();
					gameObject.SetActive (false);
					Pause.returnMenu = "pause";
				} else {
					Time.timeScale = 1;
					gameObject.SetActive (false);
				}
			}
		}
	}

	public void ViewText(){
		viewText = true;
		transparentBckg.SetActive (true);
	}

	void SelectLastSelected(){
		foreach(Button btn in inventoryButtons){
			if(btn.gameObject.name == selectedBtn){
				btn.Select ();
				break;
			}
		}
	}

	void LoadSprites()
	{
		foreach(Sprite image in Resources.LoadAll<Sprite>("Inventory"))
		{
			inventoryImages.Add (image);
		}
	}
}
